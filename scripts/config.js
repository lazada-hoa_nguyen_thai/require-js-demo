require.config({
	deps: ['main'],
	paths: {
		'jquery': '../bower_components/jquery/dist/jquery',
		'underscore': '../bower_components/underscore/underscore',
		'backbone': '../bower_components/backbone/backbone',
		'velocity': '../bower_components/velocity/velocity',
		'text': '../bower_components/requirejs-text/text',
		'person': 'modules/person'
	}
	,
	shim: {
		velocity: {
			deps: ['jquery']
		}
	}
});