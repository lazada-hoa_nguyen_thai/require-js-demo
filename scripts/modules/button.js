define(['jquery','velocity'],function($, Velocity, Section) {
	console.log("BUTTON");

 	var $document = $(document);

 	var Button = function($el) {
 		this.$el = $el;
 		this.$el.data('instance', this);
 		this.clicked = false;
 		this.bindEvents();
 	}

 	Button.prototype = {
 		bindEvents: function() {
 			var that = this;

 			this.$el.on('click', function(e) {
 				e.stopPropagation();
				e.preventDefault();
				var marginLeft = that.clicked ? '0px': '200px';
			 	that.move.apply(that, [marginLeft]);
 			});

			$document.on('click', function() {
				if(!that.clicked) return false;
				that.move.apply(that, ['0px']);
			});

 		},
 		move: function(marginLeft) {
 			this.clicked = !this.clicked;
			this.$el.velocity({marginLeft: marginLeft});
 		}
 	};	

 	return Button;
});