define(['backbone', 'jquery', 'underscore', 'person/component'], 

	function(Backbone, $, _, Person) {

		var View = Backbone.View.extend({
			tagName: 'ul',
			className: 'company',
			id: 'company',
			initialize: function() {
			},
			render: function() {
				var that = this;
				this.collection.forEach(function(model) {
					var personView = new Person.View({
						model: model
					});

					that.$el.append(personView.render().el);
				});
				return this;
			}
		});

		return View;

	}
);