define(['backbone', 'person/component'],function(Backbone, Person) {
	var Collection = Backbone.Collection.extend({
		model: Person.Model
	});
	
	return Collection;
});