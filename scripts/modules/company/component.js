define(['./model', './view', 'jquery'], 
	function(Model, View, $) {
		return {
			init: function() {
				var collection = new Model([
					{last: 'Nguyen', first: 'Thai Hoa'},
					{last: 'Nguyen', first: 'Thai Phong'},
					{last: 'Nguyen', first: 'Thai Duong'},
					{last: 'Nguyen', first: 'Thai Thuan'},
				]);

				var companyView = new View({
					collection: collection
				});

				$('.section').append(companyView.render().el);
			}
		};
	}
);