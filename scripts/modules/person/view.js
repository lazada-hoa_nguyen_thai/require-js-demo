define(['backbone', 'jquery', 'text!person/template.html'],function(Backbone, $, personTemplate) {

	
	var View = Backbone.View.extend({
		tagName: 'li',
		template: _.template(personTemplate),
		events: {
			'click strong': 'showText'
		},
		initialize: function() {
		},
		render: function() {
			var html = this.template(this.model.toJSON());
			this.$el.append(html);
			return this;
		},
		showText: function(e) {
			alert($(e.target).data('value'));
		}
	});

	return View;

});