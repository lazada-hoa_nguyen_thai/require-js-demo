define(['person/model', 'person/view'],function(Model, View) {
	return {
		Model: Model,
		View: View	
	};

});