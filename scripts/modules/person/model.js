define(['backbone'],function(Backbone) {
	var Model = Backbone.Model.extend({
		defaults: {
			first: '',
			last: ''
		}
	});
	
	return Model;
});