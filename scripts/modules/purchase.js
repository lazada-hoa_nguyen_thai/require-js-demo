define(['modules/credit', 'modules/product'], function (credit, product) {
	console.log("Function: purchase");

	return {
		purchaseProduct: function() {
			var credits = credit.get();
			if (credits > 0) {
				product.reserve();
				return true;
			}
			return false;
		}
	};
	
});