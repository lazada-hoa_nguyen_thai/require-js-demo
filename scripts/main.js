require(
	[
		'modules/purchase', 
		'modules/button',
		'modules/section',
		'modules/company/component',
		'jquery'
	], 
	function(purchase, Button, Section, Company,$) {
		console.log("MAIN FILE LOADED");
		$(function() {

			console.log("DOM READY");

			$('.button').each(function() {
				new Button($(this));
			});

			Section.init();

			Company.init();
		})
	}
);