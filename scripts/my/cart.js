define(function () {
	var cartItems = [];
	return {
		add: function(item) {
			cartItems.push(item);
		},
		getCartItems: function() {
			return cartItems;
		}
	}
})