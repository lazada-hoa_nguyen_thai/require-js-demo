define(['./cart'], function(cart) {
	return {
		size: 'L',
		color: 'red',
		addToCart: function() {
			cart.add(this);
			console.log(cart.getCartItems());
		}
	}
});