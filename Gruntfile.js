module.exports = function(grunt) {
	var cssTemplate = function(param) {
		var arrStr = [];
		arrStr.push('.sprite {\n');
		arrStr.push('\tbackground-image: url(' + param.items[0].image + ');\n');
		arrStr.push('\tborder: none;\n');
		arrStr.push('\toutline: none;\n');
		arrStr.push('\tbackground-repeat: no-repeat;\n');
		arrStr.push('\ttext-indent: -9999px;\n');
		arrStr.push('\tdisplay: inline-block;\n');
		arrStr.push('}\n');
		param.items.forEach(function(item) {
			arrStr.push('.sprite.' + item.name + ' {\n');
			arrStr.push('\tbackground-position: -' + item.px.x + ' -' + item.px.y + ';\n');
			arrStr.push('\twidth:' + item.px.width + ';\n');
			arrStr.push('\theight:' + item.px.height + ';\n');
			arrStr.push('}\n');
		});
		return arrStr.join('');
	};

	var configs = {
		wiredep: {
			useBower: {
				src: ['*.html']
			}
		},
		watch: {
			configFiles: {
				files: ['Gruntfile.js']
			},
			reload: {
				files: [ 'scripts/**/*.js', '*.html'],
				options: {
					livereload: true
				}
			},
			sprites: {
				files: ['images/sprites/*.{png,jpg}'],
				tasks: ['sprite']
			}
			// bower: {
			// 	files: ['bower.json'],
			// 	tasks: ['wiredep']
			// }
		},
		sprite: {
			all: { 
				src: ['images/sprites/*.{png,jpg}'],
				destImg: 'images/sprites.png',
				destCSS: 'css/sprites.css',
				padding: 2,
				cssTemplate: cssTemplate
			}
		}
		,
		connect: {
			server: {
				options: {
					port: 8080,
					livereload: true,
					hostname: 'localhost',
					middleware: function(connect, options) {
						return [
							require('connect-livereload')(),
							connect.static(options.base[0])
						]
					}
				}
			}
		}
	};

	grunt.config.init(configs);

	grunt.loadNpmTasks('grunt-wiredep');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-spritesmith');

	grunt.task.registerTask('default', ['sprite','connect', 'watch'])
};
